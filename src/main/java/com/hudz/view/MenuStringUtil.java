package com.hudz.view;

import com.hudz.model.StringUtil;

import java.util.*;

public class MenuStringUtil extends ConsoleMenu {

    String text;
    String[] sentence;
    String[][] words;

    private void setMenu() {
        this.menu = new LinkedHashMap<>();
        this.menu.put("1", bundle.getString("1"));
        this.menu.put("2", bundle.getString("2"));
        this.menu.put("3", bundle.getString("3"));
        this.menu.put("4", bundle.getString("4"));
        this.menu.put("5", bundle.getString("5"));
        this.menu.put("6", bundle.getString("6"));
        //this.menu.put("7", bundle.getString("7"));
        this.menu.put("8", bundle.getString("8"));
        //this.menu.put("9", bundle.getString("9"));
        //this.menu.put("10", bundle.getString("10"));
        //this.menu.put("11", bundle.getString("11"));
        this.menu.put("12", bundle.getString("12"));
        //this.menu.put("13", bundle.getString("13"));
        this.menu.put("14", bundle.getString("14"));
        //this.menu.put("15", bundle.getString("15"));
        this.menu.put("16", bundle.getString("16"));
        this.menu.put("18", bundle.getString("18"));
        this.menu.put("19", bundle.getString("19"));
    }

    public MenuStringUtil() throws Exception {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("StringUtilMenu", locale);
        setMenu();
        this.menu.put("Q", bundle.getString("Q"));

        this.methodsMenu.put("1", this::printMaxCountSentenceWithSameWords);
        this.methodsMenu.put("2", this::printOrderedSentenceByLength);
        this.methodsMenu.put("3", this::printUniqueWordFirstSentence);
        this.methodsMenu.put("4", this::printWordsWithLengthInQuestions);
        this.methodsMenu.put("5", this::printSwappedWordsStartsWithVowelWithLongest);
        this.methodsMenu.put("6", this::printWordsInAlphabetOrder);
        this.methodsMenu.put("7", this::sortWordsByIncreasingPercentageOfVowels);
        this.methodsMenu.put("8", this::printSortedVowelWord);
        //this.methodsMenu.put("9", this::getOccurance);
        //this.methodsMenu.put("10", this::getOccurance);
        //this.methodsMenu.put("11", this::getOccurance);
        this.methodsMenu.put("12", this::printTextWithDeletedWordsStartsWithConsonantAndSettedLength);
        //this.methodsMenu.put("13", this::getOccurance);
        this.methodsMenu.put("14", this::printMaxLengthPolindrom);
        //this.methodsMenu.put("15", this::getOccurance);
        this.methodsMenu.put("16", this::printSwappedSubstringWithLengthAndSettedWord);
        this.methodsMenu.put("18", this::internationalizeMenuUkrainian);
        this.methodsMenu.put("19", this::internationalizeMenuEnglish);
        setWords();
    }
    private void setWords() throws Exception {
        text = controller.getText();
        sentence = controller.getSentence(text);
        words = new String[sentence.length][];
        for (int i = 0; i < sentence.length; i++) {
            words[i] = controller.getWords(sentence[i]);
        }
    }
    @Override
    public void show() {
        super.show();
    }
    private void internationalizeMenuUkrainian() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("StringUtilMenu", locale);
        setMenu();
        show();
    }
    private void internationalizeMenuEnglish() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("StringUtilMenu", locale);
        setMenu();
        show();
    }
    //1
    private void printMaxCountSentenceWithSameWords() {
        logger.trace(bundle.getString("17")+ "\n" + text);
        logger.trace("Max count of sentence with the same word = " + controller.getMaxCountSentenceWithSameWords(words));
    }
    //2
    private void printOrderedSentenceByLength() {
        logger.trace(bundle.getString("17")+ "\n" + text);
        logger.trace("\n" + bundle.getString("21"));
        String[] ordered = controller.orderSentenceByLength(sentence);
        for (String s: ordered) {
            logger.trace(s);
        }
    }
    //3
    private void printUniqueWordFirstSentence() {
        logger.trace(bundle.getString("17")+ "\n" + text);
        logger.trace("\n" + bundle.getString("22"));
        String word = controller.getUniqueWordFirstSentence(words);
        logger.trace("First unique word in 1st setnence: " + word);
    }
    //4
    private void printWordsWithLengthInQuestions() {
        logger.trace(bundle.getString("17")+ "\n" + text);
        //logger.trace("Please, input the length for searching:");
        //TODO: users input
        int length = 4;
        logger.trace("\nFounded words in questions with length " + length);
        logger.trace(Arrays.toString(controller.findWordsInQuestions(words, length)));
    }
    //5
    private void printSwappedWordsStartsWithVowelWithLongest() {
        logger.trace(bundle.getString("17")+ "\n" + text);
        logger.trace("\nSwap longest word with word starts with vowel");
        logger.trace(controller.getSwapedWordsVowelWithLongest(words, sentence));
    }
    //6
    private void printWordsInAlphabetOrder() {
        logger.trace(bundle.getString("17")+ "\n" + text);
        logger.trace("\nWords in alphabet order:");
        logger.trace(Arrays.toString(controller.getWordsInAlphabetOrder(words)));
    }
    //7
    private void sortWordsByIncreasingPercentageOfVowels() {
        System.out.println("\nSorry nothing here");
    }
    //8
    private void printSortedVowelWord() {
        logger.trace(bundle.getString("17")+ "\n" + text);
        logger.trace("\nVowel list sorted by first consolant");
        logger.trace(Arrays.toString(controller.getSortedVowelWord(words)));
    }
    //12
    private void printTextWithDeletedWordsStartsWithConsonantAndSettedLength() {
        logger.trace(bundle.getString("17")+ "\n" + text);
        logger.trace("List with deleted words starts with consonant and length 4");
        logger.trace(Arrays.toString(controller.deleteLengthWordsStartsWithConsonant(words, 4)));
    }
    //14
    private void printMaxLengthPolindrom() {
        logger.trace(bundle.getString("17")+ "\n" + text);
        logger.trace("\nMax length polindom is ");
        logger.trace(controller.getMaxLengthPolindrom(words));
    }
    //16
    private void printSwappedSubstringWithLengthAndSettedWord() {
        logger.trace(bundle.getString("17")+ "\n" + text);
        logger.trace("\nSwap word with length 4 with substring AVANGERS in sentence 3");
        logger.trace(controller.getSwappedSubstringAndWord(words, 4, "AVANGERS", 3));
    }
}


package com.hudz.view;

import com.hudz.controller.*;
import com.hudz.model.StringUtil;

import java.util.*;

public class ConsoleView extends ConsoleMenu {

  MenuStringUtil menuStringUtil;

  Locale locale;
  ResourceBundle bundle;

  private void setMenu() {

    menu = new LinkedHashMap<>();
    menu.put("1", bundle.getString("1"));
    menu.put("2", bundle.getString("2"));
    menu.put("3", bundle.getString("3"));
    menu.put("Q", bundle.getString("Q"));
  }

  public ConsoleView() throws Exception {
    locale = new Locale("uk");
    bundle = ResourceBundle.getBundle("MyMenu", locale);
    setMenu();
    methodsMenu = new LinkedHashMap<>();
    menuStringUtil =  new MenuStringUtil();

    methodsMenu.put("1", this::tasksStringUtils);
    methodsMenu.put("2", this::internationalizeMenuUkrainian);
    methodsMenu.put("3", this::internationalizeMenuEnglish);
  }
  private void tasksStringUtils() {
    menuStringUtil.show();
  }

  private void internationalizeMenuUkrainian() {
    locale = new Locale("uk");
    bundle = ResourceBundle.getBundle("MyMenu", locale);
    setMenu();
    show();
  }

  private void internationalizeMenuEnglish() {
    locale = new Locale("en");
    bundle = ResourceBundle.getBundle("MyMenu", locale);
    setMenu();
    show();
  }
}

package com.hudz.model;

import java.io.FileReader;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class StringUtil {

    private List<Object> objects = new ArrayList<>();

    public StringUtil addToParameters(Object obj){
        objects.add(obj);
        return this;
    }

    public String concat(){
        StringBuilder stringBuilder = new StringBuilder();
        for (Object obj : objects){
            stringBuilder.append(obj).append("  ");
        }
        return stringBuilder.toString();
    }

    public String getText() throws Exception {
        StringBuilder text = new StringBuilder(new String());
        FileReader fr = new FileReader( "text.txt" );
        Scanner scan = new Scanner(fr);

        int i = 1;
        while (scan.hasNextLine()) {
            text.append(scan.nextLine());
            i++;
        }
        fr.close();
        return text.toString();
    }

    public String[] getSentence(String text) {
        return text.split("(?<=[.!?]) ");
    }

    public String[] getWords(String sentence) {
        String[] words = {};
        for (int i = 0; i < sentence.length(); ++i) {
            words = sentence.split(" ?(?<!\\G)((?<=[^\\p{Punct}])(?=\\p{Punct})|\\b) ?");
        }
        return words;
    }

    //task1 Знайти найбільшу кількість речень тексту, в яких є однакові слова.
    public int getMaxCountSentenceWithSameWords(String[][] words) {
        Map<String, Integer> countedWords = new HashMap<>();
        Set<String> setWords = new LinkedHashSet<>();
        for (String[] word : words) {
            setWords.addAll(Arrays.asList(word).subList(0, word.length));
        }
        for (String s: setWords) {
            int counter = 0;
            for (String[] word : words) {
                for (String s1 : word) {
                    if (s.equals(s1)) {
                        counter++;
                        break;
                    }
                }
            }
            countedWords.put(s,counter);
        }
        int maxValue = countedWords.entrySet().stream().max((entry1, entry2) -> entry1.getValue() > entry2.getValue() ? 1 : -1).get().getValue();
        return maxValue;
    }
    //task2 Вивести всі речення заданого тексту у порядку зростання кількості слів у
    //кожному з них.
    public String[] orderSentenceByLength(String[] sentences) {
        String[] orderedSentence = sentences.clone();
        for(int i = 0; i < orderedSentence.length; i++){
            for(int j = 0; j < orderedSentence.length - 1 - i; j++){
                if(orderedSentence[j].length() > orderedSentence[j + 1].length()){
                    String str = orderedSentence[j];
                    orderedSentence[j] = orderedSentence[j + 1];
                    orderedSentence[j + 1] = str;
                }
            }
        }
        return orderedSentence;
    }
    //task 3 Знайти таке слово у першому реченні, якого немає ні в одному з інших
    //речень.
    public String getUniqueWordFirstSentence(String[][] words) {
        String word = "No unique words in 1st sentence";
        boolean unique;
        for (int k = 0; k < words[0].length; k++) {
            unique = true;
            for (int i = 1; i < words.length; i++) {
                for (int j = 0; j < words[i].length; j++) {
                        if(words[0][k].toLowerCase().equalsIgnoreCase(words[i][j])|| (words[0][k].matches("[!.?,]"))) {
                            unique = false;
                            break;
                        }
                    }
                }
            if(unique) {
                return words[0][k];
            }
        }
        return word;
    }
    //task4 У всіх запитальних реченнях тексту знайти і надрукувати без повторів
    //слова заданої довжини.
    public String[] findWordsInQuestions(String[][] words, int length) {
       Set<String> wordsWithLength = new HashSet<>();
        for (String[] word : words) {
            for (int j = 0; j < word.length; j++) {
                if (word[word.length - 1].equals("?")) {
                    if (word[j].length() == length) {
                        wordsWithLength.add(word[j]);
                    }
                }
            }
        }
        return wordsWithLength.stream().toArray(String[]::new);
    }

    //task5 У кожному реченні тексту поміняти місцями перше слово, що починається
    //на голосну букву з найдовшим словом.
    public String swapWordsVowelWithLongest(String[][] words, String[] sentences) {
        String[][] swapedWords = words.clone();
        for (int i = 0; i < words.length; i++) {
            int indexOfVowelWord = getIndexOfFirstWordVowel(words[i]);
            int indexOfLongestWord = getIndexOfLongestWord(words[i]);
            if(indexOfVowelWord != -1) {
                    String temp = words[i][indexOfLongestWord];
                    swapedWords[i][indexOfLongestWord] = swapedWords[i][indexOfVowelWord];
                    swapedWords[i][indexOfVowelWord] = temp;
                }
            }
        StringBuilder text = new StringBuilder();
        for (int i = 0; i < swapedWords.length; i++) {
            for (int j = 0; j < swapedWords[i].length; j++) {
                if((j != swapedWords[i].length - 1)&&swapedWords[i][j + 1].matches("[!,?.]")) {
                    text.append(swapedWords[i][j]);
                } else {
                    text.append(swapedWords[i][j]).append(" ");
                }
            }
        }
        return text.toString();
    }

     private static int getIndexOfFirstWordVowel(String[] word) {
         Pattern pattern = Pattern.compile("^[aequio]");
         return getIndexOfFirstMatch(word,pattern);
     }

    public static int getIndexOfFirstWordСonsonantal(String[] word) {
        Pattern pattern = Pattern.compile("^[wrtpsdghklzxcvbnm]");
        return getIndexOfFirstMatch(word, pattern);
    }

    private static int getIndexOfFirstMatch(String[] word, Pattern pattern) {
        int indexOfMatch = -1;
        for (int j = 0; j < word.length; j++) {
            Matcher matcher = pattern.matcher(word[j]);
            if (matcher.find()) {
                indexOfMatch = j;
                break;
            }
        }
        return indexOfMatch;
    }

     private static int getIndexOfLongestWord(String[] word) {
         int indexOfLongestWord = -1;
         String longestWord = " ";
         for (int k = 0; k < word.length; k++) {
             if(word[k].length()> longestWord.length()) {
                 longestWord = word[k];
                 indexOfLongestWord = k;
             }
         }
        return indexOfLongestWord;
     }

    //task6 Надрукувати слова тексту в алфавітному порядку по першій букві. Слова,
    //що починаються з нової букви, друкувати з абзацного відступу.
    public String[] getWordsInAlphabetOrder (String[][] words) {
        List<String> wordsList = Arrays.stream(words)
                .flatMap(Arrays::stream).sorted().collect(Collectors.toList());
        StringBuilder[] sortedWords = new StringBuilder[wordsList.size()];

        for (int i = 0; i < wordsList.size() ; i++) {
            if (wordsList.get(i).charAt(0) > 'A' && wordsList.get(i).charAt(0) < 'Z') {
                sortedWords[i] = new StringBuilder("\t" + wordsList.get(i).subSequence(0, wordsList.get(i).length()));
            } else sortedWords[i] = new StringBuilder(wordsList.get(i).subSequence(0, wordsList.get(i).length()));
        }
        String[] result = new String[sortedWords.length];
        for (int i = 0; i < result.length ; i++) {
            result[i] = sortedWords[i].toString();
        }
        return result;
    }

    //task7 Відсортувати слова тексту за зростанням відсотку голосних букв
    //(співвідношення кількості голосних до загальної кількості букв у слові).



    //task8 Слова тексту, що починаються з голосних букв, відсортувати в
    //алфавітному порядку по першій приголосній букві слова.
    public String[] getSortedVowelWord (String[][] words) {
        List<String> wordList = Arrays.stream(words)
                .flatMap(Arrays::stream).sorted(CompareByFirstConsonant.getInstance()).collect(Collectors.toList());
        wordList.removeIf(s -> !startsWithVowel(s));
        return  wordList.toArray(new String[0]);
    }

    private static boolean startsWithVowel(String s) {
        return s.length() > 1 && !CompareByFirstConsonant.isConsonant(s.charAt(0));
    }

    //task 9 Всі слова тексту відсортувати за зростанням кількості заданої букви у
    //слові. Слова з однаковою кількістю розмістити у алфавітному порядку.

    //task10 Є текст і список слів. Для кожного слова з заданого списку знайти, скільки
    //разів воно зустрічається у кожному реченні, і відсортувати слова за
    //спаданням загальної кількості входжень.

    //task11 У кожному реченні тексту видалити підрядок максимальної довжини, що
    //починається і закінчується заданими символами.

    //task12 З тексту видалити всі слова заданої довжини, що починаються на
    //приголосну букву.
    public String[] deleteLengthWordsStartsWithConsonant(String[][] words, int length) {
        List<String> wordsList = Arrays.stream(words)
                .flatMap(Arrays::stream)
                .collect(Collectors.toList());
        wordsList.removeIf(str -> !startsWithVowel(str) || str.matches("[\\s?!.,]"));
        return wordsList.toArray(new String[0]);
    }

    //task13 Відсортувати слова у тексті за спаданням кількості входжень заданого
    //символу, а у випадку рівності – за алфавітом.

    //task14 У заданому тексті знайти підрядок максимальної довжини, який є
    //паліндромом, тобто, читається зліва на право і справа на ліво однаково.
    public String getMaxLengthPolindrom(String[][] words) {
        List<String> polindromes = new ArrayList<>();
        for (String[] word : words) {
            for (String s : word) {
                if (isPalindrome(s)) {
                    polindromes.add(s);
                }
            }
        }
        int maxPolindomes = getIndexOfLongestWord(polindromes.toArray(new String[0]));
        return polindromes.get(maxPolindomes);
    }

    private static boolean isPalindrome(String word) {
        if (word.length()>=2) {
            return word.equalsIgnoreCase(new StringBuilder(word)
                    .reverse().toString());
        } else return false;
    }

    //task15 Перетворити кожне слово у тексті, видаливши з нього всі наступні
    //(попередні) входження першої (останньої) букви цього слова.


    //task16 У певному реченні тексту слова заданої довжини замінити вказаним
    //підрядком, довжина якого може не співпадати з довжиною слова.
    public String swapSubstringAndWord(String[][] words, int length, String subString, int sentenceNumber) {
        String[] swapedWordsSentence = words[sentenceNumber - 1].clone();
        for (int i = 0; i < swapedWordsSentence.length; i++) {
            if(swapedWordsSentence[i].length() == length) {
                swapedWordsSentence[i] = subString;
            }
        }
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < swapedWordsSentence.length; i++) {
                if((i != swapedWordsSentence.length - 1)&&swapedWordsSentence[i + 1].matches("[!,?.]")) {
                    result.append(swapedWordsSentence[i]);
                } else {
                    result.append(swapedWordsSentence[i]).append(" ");
                }
            }
        return result.toString();
    }
}

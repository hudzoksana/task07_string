package com.hudz;

import com.hudz.view.ConsoleView;

public class Application {

  public static void main(String[] args) throws Exception {
    new ConsoleView().show();
  }
}

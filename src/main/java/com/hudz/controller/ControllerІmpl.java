package com.hudz.controller;

import com.hudz.model.*;

public class ControllerІmpl implements Controller {
  private StringUtil stringUtil;

  public ControllerІmpl() {
    stringUtil = new StringUtil();
  }
  //1
  @Override
  public int getMaxCountSentenceWithSameWords(String[][] words) {
    return stringUtil.getMaxCountSentenceWithSameWords(words);
  }
  //2
  @Override
  public String[] orderSentenceByLength(String[] sentence) {
    return stringUtil.orderSentenceByLength(sentence);
  }
  //3
  @Override
  public String getUniqueWordFirstSentence(String[][] words) {
    return stringUtil.getUniqueWordFirstSentence(words);
  }
  //4
  @Override
  public String[] findWordsInQuestions(String[][] words, int length) {
    return stringUtil.findWordsInQuestions(words, length);
  }
  //5
  @Override
  public String getSwapedWordsVowelWithLongest(String[][] words, String[] sentence) {
    return stringUtil.swapWordsVowelWithLongest(words, sentence);
  }
  //6
  @Override
  public String[] getWordsInAlphabetOrder(String[][] words) {
    return stringUtil.getWordsInAlphabetOrder(words);
  }
  //8
  @Override
  public String[] getSortedVowelWord(String[][] words) {
    return stringUtil.getSortedVowelWord(words);
  }
  //12
  @Override
  public String[] deleteLengthWordsStartsWithConsonant(String[][] words, int i) {
    return stringUtil.deleteLengthWordsStartsWithConsonant(words, i);
  }

  @Override
  public String getMaxLengthPolindrom(String[][] words) {
    return stringUtil.getMaxLengthPolindrom(words);
  }

  @Override
  public String getSwappedSubstringAndWord(String[][] words, int i, String avangers, int i1) {
    return stringUtil.swapSubstringAndWord(words, i, avangers, i1);
  }

  @Override
  public String getText() throws Exception {
    return stringUtil.getText();
  }

  @Override
  public String[] getSentence(String text) {
    return stringUtil.getSentence(text);
  }

  @Override
  public String[] getWords(String s) {
    return stringUtil.getWords(s);
  }
}

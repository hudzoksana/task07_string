package com.hudz.controller;

public interface Controller {
    //1
    int getMaxCountSentenceWithSameWords(String[][] words);
    //2
    String[] orderSentenceByLength(String[] sentence);

    String getUniqueWordFirstSentence(String[][] words);

    String[] findWordsInQuestions(String[][] words, int length);

    String getSwapedWordsVowelWithLongest(String[][] words, String[] sentence);

    String[] getWordsInAlphabetOrder(String[][] words);

    String[] getSortedVowelWord(String[][] words);

    String[] deleteLengthWordsStartsWithConsonant(String[][] words, int i);

    String getMaxLengthPolindrom(String[][] words);

    String getSwappedSubstringAndWord(String[][] words, int i, String avangers, int i1);

    String getText() throws Exception;

    String[] getSentence(String text);

    String[] getWords(String s);
}
